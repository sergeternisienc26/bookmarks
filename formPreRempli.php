<?php include("header.php") ?>
<h1>Modifier</h1>
<?php include("connexion_bdd.php") ?>


<?php

$selected = '';

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    //requête favori sélectionné suivant id
    $reponse = $bdd->prepare("SELECT * from favori where id=" . $id);
    $reponse ->execute();
    $don = $reponse->fetchAll();

    //requ
    $TableCat = $bdd->prepare("SELECT c.nom as 'nom categories', c.id as 'id categories'
    from categories as c");
      $TableCat->execute();
      $donCat = $TableCat->fetchAll();/*récupération de ttes les lignes de la requête TableCat*/

      $TableGroupe = $bdd->prepare("SELECT g.id_categories as 'id catGpe' from groupe as g  inner join categories on categories.id = id_categories where g.id_favori=" .$id);
      $TableGroupe->execute();
   
      $donGpe = $TableGroupe->fetchAll();
  
    
} else if (isset($_POST['id_favori'])) {
    $id = $_POST['id_favori'];
    $reponse = $bdd->prepare("SELECT * from favori where id=" . $id);
    $reponse ->execute();
    $don = $reponse->fetchAll();
}

?>

<h2>URL à modifier</h2>

<div class="formulaire">
    <form name="insert_lien" method="post" action="formPreRempli.php">
        <?php foreach($don as $donnees): ?>
        <div class="ligne">
            <div class="gauche"> id: </div>
            <div class="droite"> <input type="number" name="id_favori" value="<?php echo $donnees['id'] ?>" placeholder="id favori"></div>
        </div>
        <div class="ligne">
            <div class="gauche">Modifier nom: </div>
            <div class="droite"><input type="text" name="nom_favori" value="<?php echo $donnees['nom'] ?>" placeholder="nom favori"></div>
        </div>
        <div class="ligne">
            <div class="gauche">Modifier lien URL: </div>
            <div class="droite"><input type="text" name="url" value="<?php echo $donnees['url'] ?>" placeholder="entrer lien url"></div>
        </div> 
           <div class="ligne">
            <div class="gauche">Date modification: </div>
            <div class="droite"><input type="date" name="date_creation" value="<?php echo $donnees['date_creation'] ?>"></div>
        </div>
        <?php endforeach; ?>
        <div class="ligne">
        <div class="gauche"> catégorie : </div>
        <div class="droite">
          <select name="id_cat">
               <?php foreach ($donCat as $cat) :/*passe en revue tableau donnees pour assigner les valeurs à cat*/?>
                <?php foreach ($donGpe as $gpe) :?>
                <?php  
               if($cat['id categories'] == $gpe['id catGpe']){
                 $selected = 'selected="selected"';
               }
               else{
                $selected = '';
               }
               ?>
            <option <?php echo $selected ?> value='<?php echo $cat['id categories']?>'> <?php echo $cat['nom categories'] ?></option>
          <?php endforeach; /*fin du foreach*/ ?>
          <?php endforeach; ?>
          </select>
        </div>
      </div>
    
        <input class="valide" type="submit" name="valider" value="modifier" />
    </form>
</div>


<?php

if (isset($_POST['valider'])) {
    $nom_favori = $_POST['nom_favori'];
    $id_favori = $_POST['id_favori'];
    $url = $_POST['url'];
    $id_cat = $_POST['id_cat'];
    $date_creation = $_POST['date_creation'];

    $reponse = 'UPDATE favori SET nom=:nom_favori, url=:url, date_creation=NOW() WHERE id =:id_favori';
    $res = $bdd->prepare($reponse);
    $exec = $res->execute(array(':nom_favori' => $nom_favori, ':url' => $url, ':id_favori' => $id_favori));

    $join = $bdd->prepare("UPDATE `groupe` SET id_categories=:id_cat where id_favori=:id_favori");
    $join->bindvalue(':id_cat', $id_cat, PDO::PARAM_STR);
    $join->bindvalue(':id_favori', $id_favori, PDO::PARAM_STR);

    $join->execute();

    $join->closeCursor();

}


if($reponse && isset($join))
{
    ?>
    <p class="delete">Favori (bookmark) modifié<br/>Vous pouvez retourner à la page d'accueil</p>
    

<?php
}
?>

</body>

</html>