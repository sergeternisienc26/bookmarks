<?php include("header.php") ?>



<?php include("connexion_bdd.php") ?>

<?php
/*requête sql création nouveau favori (bokmark)*/

?>



<div class="categories">

  <div class="insert">
    <h2>Insert nouvelle catégorie</h2>
    <div class="formulaire">
      <form name="insert_lien" method="post">
        <div class="ligne">
          <div class="gauche"> Entre nom catégorie :</div>
          <div class="droite"> <input type="text" name="nom_categorie" placeholder="nom catégorie" /> </div>
        </div>
        <div class="ligne">
          <div class="gauche"> description : </div>
          <div class="droite"> <input type="text" name="descriptionCat" placeholder="description" /> </div>
        </div>
    </div>
    <input class="valide" type="submit" name="valider" value="ok" />
  </div>

  <?php
  if (isset($_POST['valider']) ) {
    $nom_categorie = $_POST['nom_categorie'];
    $descriptionCat = $_POST['descriptionCat'];

    $reponse = $bdd->prepare("INSERT INTO `categories`(`id`, `nom`, `description`) 
    VALUES ('', :nom_categorie, :descriptionCat)");
    $reponse->bindValue(':nom_categorie', $nom_categorie, PDO::PARAM_STR);
    $reponse->bindValue(':descriptionCat', $descriptionCat, PDO::PARAM_STR);
    $reponse->execute();
    $reponse->closeCursor();
  }
  ?>
  
<?php

$rep= $bdd->prepare("SELECT c.nom as 'nom categories', c.id as 'id categories', c.description as 'description'from categories as c");
$rep->execute();
$donnees = $rep->fetchAll();
$rep->closeCursor();
?>
    <div class="tabCat">
    <h2>Liste Catégories</h2>
    
    <table>
      <tr>
        <th>Nom</th>
        <th>Description</th>
        <th>Modifier</th>
        <th>Sup.</th>
      </tr>
      <tr> 
        <?php foreach ($donnees as $result) : ?>
          <td><?php echo $result['nom categories'] ?></td>
          <td><?php echo $result['description'] ?></td>
          <td><a href=<?php echo 'formcatmodif.php?id=' . $result['id categories'] ?>> <i class="far fa-edit" ></i></a></td>
          <td><a href=<?php echo 'formcatdelete.php?id=' . $result['id categories'] ?>><i class="fas fa-user-slash"></i></a></td>
      </tr>  
      <?php endforeach; ?>
    </table>

  </div>
</div>
<?php include('footer.php'); ?>