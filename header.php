<?php include("connexion_bdd.php"); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>bookmark</title>
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <header>
        <h1><?php echo "LIENS FAVORIS (BOOKMARKS)" ?></h1>
        <div class="menu">
            <nav>
                <ul>
                    <li><?php echo "<a href='\bookmarks/index.php'>Accueil Retour</a>"; ?></li>
                    <li> <?php echo "<a href='\bookmarks/formulaire.php'>Ajouter favori </a>"; ?> </li>
                    <li><?php echo "<a href='\bookmarks/formcat.php'>catégories liste-ajouter-modifier</a>"; ?></li>
                </ul>
            </nav>

            <div class="recherche">
                <form name="insert_lien" method="post">
                    <?php
                    $Tablefav = $bdd->prepare("SELECT f.nom as 'nom favori', f.id as 'id favori'from favori as f ORDER BY f.nom");
                    $Tablefav->execute();
                    $donfav = $Tablefav->fetchAll();

                    ?>
                    <select name="id_fav">
                        <option value="">favori : </option>
                        <?php foreach ($donfav as $fav) :/*passe en revue tableau donnees pour assigner les valeurs à cat*/ ?>
                            <option value='<?php echo $fav['id favori'] ?>'><?php echo $fav['nom favori'] ?></option>
                        <?php endforeach; /*fin du foreach*/ ?>
                    </select>
                    <input class="valide" type="submit" name="recherche" value="recherche" />
                </form>
            </div>


        </div>

        <?php
        if (isset($_POST['recherche']) && isset($_POST['id_fav'])) {
            $idfav = $_POST['id_fav'];
            $rech = $bdd->prepare("SELECT c.nom as 'nomcat', favori.id, favori.nom, favori.url
            from favori 
            left join groupe as g ON favori.id = g.id_favori
            left join categories as c ON c.id = g.id_categories
            where favori.id=:idfav");
            $rech->bindvalue(':idfav', $idfav, PDO::PARAM_STR);
            $rech->execute();
            $rechfav = $rech->fetchAll();

            foreach ($rechfav as $result) : ?>

                <div class="card" id="<?php echo $result['id'] ?>">
                    <h2>nom favori : <?php echo $result['nom'] ?></h2>
                    <p> categorie : <?php echo $result['nomcat'] ?></p>

                    <a href=<?php echo $result['url'] ?> target="_blank">aller à</a>
                    <a href=<?php echo 'formPreRempli.php?id=' . $result['id'] ?>>modifier</a>
                    <a href=<?php echo 'formDelete.php?id=' . $result['id'] ?>>supprimer</a>
                </div>
            <?php endforeach; ?>
        <?php
        }

        ?>
    </header>