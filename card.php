<?php

$reponse = $bdd->prepare("SELECT c.nom as 'nom categories', f.id as 'id favori', f.url as 'url favori', f.nom as 'nom favori', f.date_creation as 'date creation'
from favori as f
left join groupe as g ON f.id = g.id_favori
left join categories as c ON c.id = g.id_categories");
$reponse->execute();
$donnees = $reponse->fetchAll();
?>

<?php foreach ($donnees as $result) : ?>

    <div class="card" id="<?php echo $result['id favori'] ?>">
        <h2>nom favori : <?php echo $result['nom favori'] ?></h2>
        
        <p> categorie : <?php echo $result['nom categories'] ?></p>
        <p>lien Favoris : <?php echo $result['url favori'] ?></p>
        <p>date création : <?php echo $result['date creation'] ?></p>

        <a href=<?php echo $result['url favori'] ?> target="_blank">aller à</a>
        <a href=<?php echo 'formPreRempli.php?id=' . $result['id favori'] ?>>modifier</a>
        <a href=<?php echo 'formDelete.php?id=' . $result['id favori'] ?>>supprimer</a>
    </div>
<?php endforeach; ?>