<?php include("header.php") ?>



<?php include("connexion_bdd.php") ?>
         
<?php /*formulaire dynamique insertion nouveau favori(bookmark)*/include("formbookmakDyn.php") ?>


<?php
/*requête sql création nouveau favori (bokmark)*/

if (isset($_POST['valider']))
 {
    $nom_favori = $_POST['nom_favori'];
    $url = $_POST['url'];
    $date_creation = $_POST['date_creation'];
    $id_cat = $_POST['id_cat'];


    $reponse = $bdd->prepare("INSERT INTO `favori`(`id`, `nom`, `url`, `date_creation`) 
    VALUES ('', :nom_favori, :url, NOW())");
    $reponse->bindValue(':nom_favori', $nom_favori, PDO::PARAM_STR);
    $reponse->bindValue(':url', $url, PDO::PARAM_STR);

    $reponse->execute();

    /*appel id dernier favori(bookmark) créé*/
    $lastid = $bdd->lastInsertId();

    /*liaison dans table groupe favori à categories via id*/

        $join = $bdd->prepare("INSERT INTO `groupe` (`id_favori`, `id_categories`)  VALUES (:lastid, :id_cat);");
        $join->bindvalue(':lastid', $lastid, PDO::PARAM_STR);
        $join->bindvalue(':id_cat', $id_cat, PDO::PARAM_STR);

        $join->execute();

        $join->closeCursor();
    
    $reponse->closeCursor();
 }
    if(isset($reponse) && isset($join))
    {
    ?>
     <p class="endcreate">favori créé <br/>vous pouvez retouner à la page accueil </p>
        <?php
    }
    ?>

<?php include("footer.php") ?>