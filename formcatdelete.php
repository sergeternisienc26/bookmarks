<?php include "header.php"; ?>

<?php include("connexion_bdd.php") ?>

<?php

$id=$_GET["id"];

$suppCat = $bdd ->prepare('SELECT favori.nom as favnom, favori.id
FROM favori
INNER JOIN groupe ON groupe.id_favori = favori.id
WHERE groupe.id_categories = :id');
$suppCat->bindValue(':id', $id, PDO::PARAM_INT );
$suppCat->execute();
$categ = $suppCat->fetchAll();

try {
    
$delcat=$bdd-> prepare("DELETE FROM categories WHERE id= :id");/*supprime la catégorie*/
$delcat->bindValue(':id', $id, PDO::PARAM_INT);
$delcat->execute();
echo '<p class="delete">catégorie supprimée<br/>Vous pouvez vous en retourner où bon vous semble</p>';

} catch (Exception $e) {

    echo '<p class="delete">catégorie non supprimée<br/> Vérifié les bookmark :<br/>';

    foreach($categ as $catdel) {
        echo $catdel['favnom']. '<br/>' ;
    }
    echo '</p>' ;

}


include "footer.php";