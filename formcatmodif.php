<?php include("header.php") ?>
<?php include("connexion_bdd.php") ?>

<?php 

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    //requête favori sélectionné suivant id
    $reponse = $bdd->prepare("SELECT * from categories where id=" . $id);
    $reponse ->execute();
    $don = $reponse->fetchAll();
}

else if (isset($_POST['id_categories'])) {
    $id = $_POST['id_categories'];
    $reponse = $bdd->prepare("SELECT * from categories where id=" . $id);
    $reponse ->execute();
    $don = $reponse->fetchAll();
}

?>

<h2>catégorie à modifier</h2>

<div class="formulaire">
    <form name="insert_lien" method="post" >
        <?php foreach($don as $donnees): ?>
        <div class="ligne">
            <div class="gauche"> id: </div>
            <div class="droite"> <input type="number" name="id_categories" value="<?php echo $donnees['id'] ?>" placeholder="id categories" ></div>
        </div>
        <div class="ligne">
            <div class="gauche">nom: </div>
            <div class="droite"><input type="text" name="nom_categories" value="<?php echo $donnees['nom'] ?>" placeholder="no change"></div>
        </div>
        <div class="ligne">
            <div class="gauche">Modifier description: </div>
            <div class="droite"><input type="text" name="description" value="<?php echo $donnees['description'] ?>" placeholder="nouvelle description"></div>
        </div> 
        <?php endforeach ?>
        <input class="valide" type="submit" name="valider" value="modifier" />
    </form>
</div>

<?php
if (isset($_POST['valider'])) {
    $nom_categories = $_POST['nom_categories'];
    $id_categories= $_POST['id_categories'];
    $description = $_POST['description'];
    
    $reponse = "UPDATE categories SET nom =:nom_categories, description=:description WHERE id =:id_categories";
    $res = $bdd->prepare($reponse);
    $exec = $res->execute(array(':nom_categories' => $nom_categories, ':description' => $description, ':id_categories' => $id_categories));
}

if(isset($exec))
{
    ?>
    <p class="delete">catégorie modifiée</p>
    

<?php
}
?>


<?php include("footer.php") ?>