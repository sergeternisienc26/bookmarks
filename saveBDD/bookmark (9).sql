-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 11 août 2021 à 23:13
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bookmark`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `description`) VALUES
(1, 'developpement', 'langages'),
(2, 'css', 'framework'),
(3, 'applications', 'css_aplications'),
(4, 'paint', 'photos images\r\n'),
(8, 'loisirs', 'divers loisirs détente'),
(9, 'popote', 'cuisine'),
(54, 'bateaux', 'mer croisière glouglou'),
(58, 'noël', 'fête cadeaux'),
(60, 'animaux', 'divers animaux'),
(61, 'infos', 'informatique divers infos');

-- --------------------------------------------------------

--
-- Structure de la table `favori`
--

CREATE TABLE `favori` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_creation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `favori`
--

INSERT INTO `favori` (`id`, `nom`, `url`, `date_creation`) VALUES
(1, 'bd jap', 'https://animedigitalnetwork.fr/onboarding', '2021-08-10'),
(3, 'unplash', 'https://unsplash.com/', '2021-08-10'),
(5, 'icones css', 'https://cssicon.space/#/', '2021-08-10'),
(167, 'dfb', 'https://www.cultura.com/arts-et-loisirs-creatifs/creation-bijoux.html', '2021-08-10'),
(168, 'iles tahiti', 'https://tahititourisme.fr/fr-fr/', '2021-08-10'),
(169, 'ratatouille', 'https://www.marmiton.org/recettes/recette_ratatouille_23223.aspx', '2021-08-10'),
(174, 'pounoussami', 'https://www.reunion.fr/', '2021-08-10'),
(176, 'cuirs leçon', 'https://www.esprit-cuir.fr/formation/des-cours-en-ligne/', '2021-08-10'),
(178, 'code pays', 'http://documentation.abes.fr/sudoc/formats/CodesPays.htm', '2021-08-10'),
(179, 'placeholder date', 'https://www.geeksforgeeks.org/how-to-set-placeholder-value-for-input-type-date-in-html-5/', '2021-08-10'),
(189, 'hohoho', 'https://www.perenoel.fi/', '2021-08-11'),
(190, 'popupou', 'https://www.reunion.fr/', '2021-08-11'),
(197, 'popupou', 'https://www.perenoel.fi/', '2021-08-11'),
(200, 'louping', 'https://www.pinterest.fr/', '2021-08-11'),
(208, 'glou glou', 'https://mersetbateaux.com/', '2021-08-11'),
(209, 'zoro', 'https://fr.wikipedia.org/wiki/Zorro', '2021-08-11'),
(211, 'louping', 'https://www.pinterest.fr/', '2021-08-11'),
(212, 'chats moi', 'https://www.croquetteland.com/fr/chat.html', '2021-08-11'),
(215, 'c b', 'https://www.visa.fr/payer-avec-visa/trouver-une-carte.html', '2021-08-11'),
(217, 'mop', 'https://fmaz.developpez.com/tutoriels/php/comprendre-pdo/', '2021-08-11'),
(218, 'axolotl', 'https://www.natura-sciences.com/environnement/axolotl582.html', '2021-08-11'),
(219, 'puce', 'http://www.aquivet.fr/infos-sante/peau-oreilles-pelage/les-puces-la-pulicose-et-la-dapp-', '2021-08-11'),
(220, 'puce', 'http://www.aquivet.fr/infos-sante/peau-oreilles-pelage/les-puces-la-pulicose-et-la-dapp-', '2021-08-11'),
(228, 'bourvil defunes', 'https://petitpaume.com/article/rire-bon-pour-la-sante', '2021-08-11');

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` int(11) NOT NULL,
  `id_favori` int(11) NOT NULL,
  `id_categories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `id_favori`, `id_categories`) VALUES
(2, 1, 4),
(3, 3, 4),
(4, 5, 2),
(163, 167, 3),
(164, 168, 8),
(165, 169, 9),
(170, 174, 8),
(172, 176, 4),
(174, 178, 9),
(175, 179, 4),
(185, 189, 58),
(186, 190, 4),
(193, 197, 58),
(196, 200, 1),
(204, 208, 54),
(205, 209, 58),
(207, 211, 4),
(208, 212, 60),
(211, 215, 61),
(213, 217, 61),
(214, 218, 60),
(215, 219, 9),
(216, 220, 9),
(224, 228, 61);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favori`
--
ALTER TABLE `favori`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_favori` (`id_favori`),
  ADD KEY `id_categories` (`id_categories`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT pour la table `favori`
--
ALTER TABLE `favori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD CONSTRAINT `groupe_ibfk_1` FOREIGN KEY (`id_favori`) REFERENCES `favori` (`id`),
  ADD CONSTRAINT `groupe_ibfk_2` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
